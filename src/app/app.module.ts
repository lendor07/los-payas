import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// PAGINAS

import { HomeComponent } from './pages/home/home.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { AyudanosComponent } from './pages/ayudanos/ayudanos.component';
import { ConocenosComponent } from './pages/conocenos/conocenos.component';

// PIPES
import { DomseguroPipe } from './pipes/domseguro.pipe';

// COMPONENTES

import { NavbarComponent } from './components/navbar/navbar.component';
import { SliderComponent } from './components/slider/slider.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { ModalComponent } from './components/modal/modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EnviarMailService } from './service/enviarMail.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { EventosComponent } from './pages/eventos/eventos.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { EventosImagenesComponent } from './pages/eventos-imagenes/eventos-imagenes.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactoComponent,
    AyudanosComponent,
    DomseguroPipe,
    ConocenosComponent,
    NavbarComponent,
    SliderComponent,
    FooterComponent,
    FormularioComponent,
    ModalComponent,
    EventosComponent,
    GaleriaComponent,
    EventosImagenesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [EnviarMailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
