import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EnviarMailService } from '../../service/enviarMail.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  name: string;
  apellido: string;
  asunto: string;
  mail: string;
  consulta: string;
  contact = { name: "", apellido: "", email: "", asunto: "", consulta: "" };
  form: FormGroup;
  
  constructor(private serviceEnviarMail: EnviarMailService) { 
    this.form = new FormGroup({
      name: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      apellido: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      asunto: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      consulta: new FormControl('', [ Validators.required, Validators.pattern('[a-zA-Z]*')]),
      email: new FormControl('', [Validators.required, Validators.pattern('^[^@]+@[^@]+\.[a-zA-Z]{2,}$')]),
    });

    this.form.setValue(this.contact, {onlySelf: true});
  }

  ngOnInit() {
  }

  enviarMail() {
    this.serviceEnviarMail.enviarMail(this.contact);
  }
}
