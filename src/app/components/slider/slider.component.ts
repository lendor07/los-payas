import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  constructor() { }
 @Input() imagenes;
 @Input() mostrarBtnAyuda;
 
  ngOnInit() {
    console.log(this.imagenes);
  }

}
