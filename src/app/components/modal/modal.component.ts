import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  nombreRol: string;
  rutaImagenRol: string;
  rutaTituloRol: string;

  @Input() visible: boolean;
  @Output() mostrar: EventEmitter<boolean>;

  constructor() {
    this.mostrar = new EventEmitter();
  }
  ngOnInit() {
    console.log(this.visible);
  }

  salida() {
    console.log(this.visible);

    this.mostrar.emit(!this.visible);
  }

}
