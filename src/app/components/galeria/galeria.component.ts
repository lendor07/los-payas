import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {

  visible: boolean;
  
  urlImagen: string;
  @Input() imagen: boolean;
  @Input() imagenes = [];
  @Input() mostrarBtnAyuda: boolean;

  constructor() {
    this.visible = false;
  }

  ngOnInit() {
  }

  salida() {
    this.visible = false;
  }
  mostrarImagen(url: string) {
    this.urlImagen = url;
    this.visible = true;
    console.log(url);
  }
}
