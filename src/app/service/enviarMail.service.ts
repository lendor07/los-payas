import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

@Injectable()
export class EnviarMailService {

    formData: any = new FormData();
    constructor(private http: HttpClient){}


    enviarMail(form){
        return this.http.post('https://www.lospayas.org.ar/sendMail/contacto.php', form)
        .subscribe(result => {
            console.log(result);
        });
    }
}