import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  constructor() { }

  sliderImg = [];

  ngOnInit() {

    this.sliderImg[0] = '../../../assets/images/payas_titeres.jpg';
    this.sliderImg[1] = '../../../assets/images/quedateencasa.jpg';
    this.sliderImg[2] = '../../../assets/images/payas_abrazon.jpg';
  }

}
