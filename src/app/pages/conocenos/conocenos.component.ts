import { Component, OnInit, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { Rol } from '../../clases/Rol';

@Component({
  selector: 'app-conocenos',
  templateUrl: './conocenos.component.html',
  styleUrls: ['./conocenos.component.scss']
})
export class ConocenosComponent implements OnInit {

  mostrarRol: boolean;
  mensaje: string;
  rol: Rol;
  constructor() {}

  ngOnInit() {
    this.rol = new Rol();
    this.mostrarRol = false;
  }

  mostrarModal(rol) {
    this.mostrarRol = !this.mostrarRol;
    console.log(rol);
    if(rol === 'civiles') {
      this.rol.nombre = 'Civiles';
    this.rol.imgTitulo = '../../../assets/images/modal/civiles-titulo.jpg';
    this.rol.imgRol = '../../../assets/images/modal/modal-civiles.jpg';
    this.rol.frase = '“Para reír de verdad, usted debe ser capaz de llevar su dolor, ¡y jugar con él!” — Charles Chaplin';
    this.rol.texto = '<b>El civil se desempeña como mediador entre el personal de salud del hospital, los niños y sus familiares, y las duplas o tríos de clowns a su cargo, cumpliendo el rol de cuidador del juego</b>. Sus funciones principales son: contacto interinstitucional, toma de decisiones respecto a cuáles son los Servicios en los que se intervendrá, las habitaciones en los que ingresará cada dúo o trío de payasos, el tiempo y duración de cada intervención, se ocupa de garantizar las medidas de bioseguridad mediante tareas de profilaxis (técnica del lavado de manos, uso de alcohol en gel, etc.). En momentos grupales facilitando el desarrollo del ritmo de la intervención y la aparición del estado lúdico.';
    }
    else {
      this.rol.nombre = 'Payasos';
      this.rol.imgTitulo = '../../../assets/images/modal/payasos-titulo.jpg';
      this.rol.imgRol = '../../../assets/images/modal/modal-payasos.jpg';
      this.rol.frase = '“La risa es un tónico, un alivio, un respiro que permite apaciguar el dolor.”— Charles Chaplin';
      this.rol.texto = '<b>El payasx de hospital busca vincularse con los niños, niñas, adolescentes, adultos acompañantes y personal de salud para ponerlos en contacto con sus propios aspectos saludables y resilientes.</b> Utilizando distintas herramientas, el clown hospitalario cumple un rol terapéutico y no meramente recreativo, en tanto funciona como puente para permitir el tránsito de los niños hacia una <b>dimensión lúdica que le permita expresar sus ansiedades, miedos, necesidades y deseos</b>, generando un intercambio placentero que trascienda el tiempo de intervención.';
    }
  }

  resultado(e) {
    console.log(e);
    this.mostrarRol = e;
}
}
