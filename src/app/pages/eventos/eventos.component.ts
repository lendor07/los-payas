import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.scss']
})
export class EventosComponent implements OnInit {

  constructor() { }


  eventos = [
    {
      id:'1',
      url:'variete-2019',
      urlImagen: '../../../assets/images/variete-2019/variete_paya.jpg',
      titulo: 'Variete 2019'
    },
    {
      id:'2',
      url:'las-magdalenas',
      urlImagen: '../../../assets/images/las-magdalenas/4.jpg',
      titulo: 'Las Magdalenas'
    },
    {
      id:'3',
      url:'paya-penia',
      urlImagen: '../../../assets/images/paya-penia/1.jpg',
      titulo: 'Paya Peña'
    },
    {
      id:'4',
      url:'recordis',
      urlImagen: '../../../assets/images/recordis/1.jpg',
      titulo: 'Recordis'
    },
    {
      id:'5',
      url:'variete-2015',
      urlImagen: '../../../assets/images/variete-2015/gracias.jpg',
      titulo: 'Variete 2015'
    },
    {
      id:'6',
      url:'clownferencias',
      urlImagen: '../../../assets/images/clownferencias/9.jpg',
      titulo: 'Clownferencias'
    }

  ];

  ngOnInit() {

  }

}
