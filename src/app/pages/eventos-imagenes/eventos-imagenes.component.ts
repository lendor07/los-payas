import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-eventos-imagenes',
  templateUrl: './eventos-imagenes.component.html',
  styleUrls: ['./eventos-imagenes.component.scss']
})
export class EventosImagenesComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  event: string;
  imagenes;
  videos;
  visible: boolean = false;
  urlImagen: string;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.event = params.get("event")
    })

    switch (this.event) {
      case 'variete-2019':

        this.imagenes = [
          {
            urlImagen: '../../../assets/images/variete-2019/variete_paya.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/vestuario_paya.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/bailongo.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/el_amor.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/don_satur.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/en-accion.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/modelando.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/paciente.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/pastafrola.jpg',
          },
          {
            urlImagen: '../../../assets/images/variete-2019/se_armo.jpg',
          }
      
        ];
        break;
    case 'las-magdalenas':
      this.imagenes = [
        {
          urlImagen: '../../../assets/images/las-magdalenas/1.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/2.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/3.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/4.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/5.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/12.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/13.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/14.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/14b.jpg',
        }
        ,
        {
          urlImagen: '../../../assets/images/las-magdalenas/15.jpg',
        },
        {
          urlImagen: '../../../assets/images/las-magdalenas/15b.jpg',
        }
      ];
      this.videos = [{
        urlImagen: 'https://www.youtube.com/embed/s1YrHKuRacs',
      },
      {
        urlImagen: 'https://www.youtube.com/embed/e9rVTf1zdKo',
      }];
    break;
case 'recordis':
  this.imagenes = [
    {
      urlImagen: '../../../assets/images/recordis/1.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/2.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/3.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/4.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/5.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/6.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/7.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/8.jpg',
    },
    {
      urlImagen: '../../../assets/images/recordis/9.jpg',
    }

  ];
  break;
  case 'paya-penia':
  this.imagenes = [
    {
      urlImagen: '../../../assets/images/paya-penia/1.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/2.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/3.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/4.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/5.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/6.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/7.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/8.jpg',
    },
    {
      urlImagen: '../../../assets/images/paya-penia/9.jpg',
    }
    ,
    {
      urlImagen: '../../../assets/images/paya-penia/10.jpg',
    }

  ];
  break;
  case 'variete-2015':
    this.imagenes = [
      {
        urlImagen: '../../../assets/images/variete-2015/ayudemos.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/bailamos.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/burbujeando.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/cantando.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/don-satur-perfil.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/dra-pastafrola.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/en-la-guardia.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/gracias.jpg',
      },
      {
        urlImagen: '../../../assets/images/variete-2015/queres-un-mate.jpg',
      }
      ,
      {
        urlImagen: '../../../assets/images/variete-2015/vacaciones.jpg',
      }
  
    ];
    break;
    case 'clownferencias':
    this.imagenes = [
      {
        urlImagen: '../../../assets/images/clownferencias/1.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/2.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/3.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/4.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/5.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/6.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/7.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/7b.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/8.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/9.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/10.jpg',
      },
      {
        urlImagen: '../../../assets/images/clownferencias/11.jpg',
      }
  
    ];
    this.videos = [{urlImagen:'https://www.youtube.com/embed/7AS-xUIATBI'}];
    break;
      default:
        break;
    }
  }


  mostrarImagen(urlImagen: string) {
    this.urlImagen = urlImagen;
    this.visible = true;
    console.log(urlImagen);
  }

}
