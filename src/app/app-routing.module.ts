import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { AyudanosComponent } from './pages/ayudanos/ayudanos.component';
import { ConocenosComponent } from './pages/conocenos/conocenos.component';
import { EventosComponent } from './pages/eventos/eventos.component';
import { EventosImagenesComponent } from './pages/eventos-imagenes/eventos-imagenes.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'conocenos', component: ConocenosComponent },
  { path: 'eventos', component: EventosComponent },
  { path: 'ayudanos', component: AyudanosComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: 'eventos/:event', component: EventosImagenesComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }